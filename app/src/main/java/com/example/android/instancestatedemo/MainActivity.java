package com.example.android.instancestatedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            int stateCount = Integer.parseInt(savedInstanceState.getString("count"));
            count = stateCount;
            Log.d(TAG, "Get count from InstanceState (onCreate): " + count);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        count++;
        Log.d(TAG,"Current count is " + count);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Add information for saving HelloToast counter
        // to the to the outState bundle
        outState.putString("count", String.valueOf(count));
        Log.d(TAG, "Save InstanceState (onSaveInstanceState): " + count);
    }

    @Override
    public void onRestoreInstanceState (Bundle mySavedState) {
        super.onRestoreInstanceState(mySavedState);

        if (mySavedState != null) {
            String stateCount = mySavedState.getString("count");
            if (stateCount != null) {
                count = Integer.parseInt(stateCount);
                Log.d(TAG, "Get count from InstanceState (onRestoreInstanceState): " + count);
            }
        }
    }

    public void pindahSecondActivity(View v){
        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
    }

}